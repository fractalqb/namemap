# namemap

[![Test Coverage](https://img.shields.io/badge/coverage-50%25-orange.svg)](file:coverage.html)
[![Go Report Card](https://goreportcard.com/badge/codeberg.org/fractalqb/namemap)](https://goreportcard.com/report/codeberg.org/fractalqb/namemap)
[![GoDoc](https://godoc.org/codeberg.org/fractalqb/namemap?status.svg)](https://godoc.org/codeberg.org/fractalqb/namemap)

`import "git.fractalqb.de/fractalqb/namemap"`

---

Map between unique names from dirfferent sources
